\chapterimage{head_repo.pdf}
\chapter{Data and Publications Repositories}
\label{ch:nos-data}
\label{ch:nos-pub}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data Acquisition and Transfer to Repositories}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\comment{Alexandros}{I believe section 2.1. serves a broader scope of 'Repository Data Governance and this is how it should be addressed IMO.}

\ncsr develops, maintains, and provisions a data repository (NOS-Data)
that serves datasets (and the metadata that describe them) acquired or
produced by research projects carried out at NCSR. NCSR also develops,
maintains, and provisions a publications repository (NOS-Pub) that
serves articles and reports prepared in the context of research
projects carried out at NCSR.

\begin{callout}
Restricting the scope of what can be served by NOS-Data and NOS-Pub
to NCSR research outputs allows these repositories to be considered
\term{institutional repositories} for the purposes of authors' right
to self-archive.
\end{callout}

\comment{Alexandros}{so are these (open access) repositories or simply archives?}

The distinction should be made between what is stored in and what is
served by NOS-Data and NOS-Pub. It is possible that practical and
technical reasons dictate that a dataset that is needed by a project
(but is not the output of this project) be stored in NOS-Data. In such
cases the dataset will only be available internally and not published
in order to safeguard the character of NOS-Data as an institutional
repository. Similarly, it is envisaged that NOS-Pub also allows
NCSR personnel to maintain and curate collections of, for instance,
articles on a given subject of interest or articles citing their own
publications. These can be hosted and made available internally, but
not published.

Within the scope defined above, the PI is responsible for identifying
which of the project data constitute datasets that need to be stored
in NOS-Data. Possible criteria for inclusion can be:
%
\begin{itemize}
\item Unrepeatability of measurements and observations; Such as of
  data collected from sensors operating in uncontrolled environments.
\item Reproducibility efficiency for measurements and observations,
  experimental results, or the results of processing that, although in
  principle reproducible, would be time and effort-consuming,
  computationally intensive, or generally inefficient to reproduce;
  Such as the outputs of weather or climate modelling.
\item Following good practices; Such as for data that substantiates
  published research findings and needs to be referenced by the
  published article regardless of its reproducibility.
\end{itemize}
%  
Other critiria can also be applied by the PI to satisfy
project-specific requirements. In any case, the PI needs to be able to
estimate in advance the approximate data volume and retainment policy
(for data that may or need only be stored for a limited time) in order
to ensure that sufficient resources can be allocated to their project.

\comment{Alexandros}{is this enforceable or on a voluntarily basis?}

The PI is responsible for uploading data and publications using the
visual user interface or the API provided by NOS-Data and NOS-Pub.
Data safety is only guaranteed after the data has been uploaded and a
backup cycle has passed, so the PI is responsible for taking data
safety measures (such as redundant copies) until the data has been
uploaded and for a short period of time after that.\footnote{See also
  Section~\ref{sec:backup} about the backup policy.}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data Organization}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

NOS-Data stores each dataset as a binary artefact and is agnostic
about the type, format, and contents of the artefact. The PI is
responsible for deciding what is the best way to partition the project
data into artefacts. This decision should take into account the
expected usage of the data so that it best balances between an overly
fine-grained partitioning and an overly coarse partitioning. If the
partitioning is too fine, it forces users to perform multiple actions
to fulfill a single data need; if it is too coarse, it forces users to
unnecessarily lengthly downloads as they only need a small fraction of
the downloaded artefact.

It is against NOS-Data policy to store copies of the same data in
order to offer different partitionings, as this wastes resources. If
different partitionings are required, the PI should notify the
operators of NOS-Data so that it can be arranged to have different
partitionings served by dynamically generating larger artefacts that
combine smaller ones without wasting storage and backup resources.
More sophisticated access methods are also described
below.\footnote{See Chapter~\ref{ch:nos-services} about deep querying
  and in-situ processing.}

Naturally, this discussion is moot in the case of publications, where
articles and reports are the obvious choice for what constitutes an
individual artefact. Organizing articles in volumes, volume series, or
any other form of collections is a matter of annotating them rather
than a matter of how they are physically organized in the repository.
Similarly, datasets can be organized into experiments, projects, or
other collections through appropriate metadata.  

Each binary artefact in either NOS-Data or NOS-Pub receives a unique
and persistent identifier. Each binary artefact with such an
identifier can be directly downloaded by resolving
this identifier. These identifiers persist in the case of updates,
with specific versions referencable suffixes or similar modifiers of
the base identifier. The base identifier resolves to the most recent
update. NOS-Pub assigns its own identifier regardless of
whether an article has been previously published or not. In the case
of self-archiving articles that have already received a DOI upon
publication, the NOS-Pub identifier resolves to the publicly available
full-text served by NOS-Pub. This serves to make the distinction
between the author-prepared version used for self-archiving and the
version-of-record of the same article. Appropriate metadata is used to
link the NOS-Pub identifier with the DOI issued by the publisher.

\begin{callout}
Satisfies requirement for persistent unique identifier.
\end{callout}

\comment{NCSR}{Buy a doi or handle prefix.}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data Safety and Security}
\label{sec:backup}\label{sec:security}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The NOS infrastructure keeps monthly backups of all its contents. This
ensures that any data at least one month old is safe from disk
failures and unauthorized or erroneous deletes and updates. Backups
are also rotated off-premises for data safety from theft or
destruction of the physical storage mediums.

\begin{callout}
A DMP is expected to describe the procedures for data recovery in the
event of an incident. \ncsr projects can refer to the data safety
measures applied by the NOS infrastructure.
\end{callout}

\comment{Alexandros}{data recovery, breach notification, access rights management and all relevant Standard Operation Processes (SOPs)}

The NOS infrastructure undergoes cybersecurity audits by
qualified experts regularly as well as after major software revisions.
There is no provision for data encryption or for distinct servers for
storing encryption keys. There is no provision for anonymization, any
anonymization steps need to be taken before transfering to the NOS
infrastructure. There is not provision for distinct servers for
keeping de-anonymization keys.

\begin{callout}
A DMP is expected to describe the procedures for data security.
\ncsr projects can refer to the data safety measures applied by the
NOS infrastructure. It is not expected that NOS-Data is appropriate
for sensitive personal data, although it is expected that the project
DPO will find the security measures in place adequate for anonymized
sensitive data.
\end{callout}

\comment{NCSR}{Decide on how this will be provisioned. This affects
  ISO 27001 Information Security, noting that this pertains to the
  infrastructure and not the applications. Applications need to be
  audited.}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Metadata}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

NOS-Data stores each dataset as a binary artefact and is agnostic
about its content, provenance, and interoperability with software and
other datasets. The only metadata automatically associated with the
artefacts are the identity of the uploader and the time of uploading.
Each project's DMP foresees the minimal metadata that should be
present for all artefacts produced by the project in order to satisfy
FAIR principles and, where appropriate, compliance with the GDPR,
licensing requirements, or any other legal and ethical requirements.
The PI is responsible for ensuring that the digital objects published
by the project are annotated with all required metadata items.

In order to support the providers of data and articles, the NOS
infrastructure provides an API and a visual user interface that are
tailored to a pre-defined set of metadata items. These items satisfy
the interpretation of FAIR and GDPR requirements presented here, but
are not technically enforced: different and additional metadata fields
can be added by users, and none of the NOS-defined metadata items are
obligatoty before the artefact can be uploaded.

\comment{Alexandros}{maybe to add the Data Act requirements (although not still there)}

\begin{callout}
The system will not refuse storing artefacts that are not fully
annotated, not annotated at all, or annotated following a different
metadata schema than the one defined for the NOS infrastructure. But
the partially-filled EC DMP template and RDA indicators provided an as
appendix to this document assume that the data provider as a minimum
provides values for all the NOS-defined metadata items.
\end{callout}
%
The NOS-defined metadata items for any artefact are the following:
%
\begin{itemize}
\item The authors or creators, the date of publication, and the title
  of the artefact.
\item The license under which the artefact is shared. This should be
  in both machine-readable and human-readable format, typically
  achieved by linking to the URL of a license.
\item Thematic terms positioning the artefact inside its scientific
  field. These must be selected from a controlled vocabulary.
  Optionally, the controlled terms can be complemented by free-text
  keywords.
\item A textual summary or description that allows users to decide if
  they need the full artefact before downloading it. For articles this
  is the abstract of the article.
\item Links to previous and subsequent versions of the same artefact,
  if any. Optionally, a description of the change between versions
  (e.g., correction of an error) allowing users to decide of it is
  necessary for their purposes to update their local copy with the new
  version.
\item Links to the (internal or collaborative) project or projects
  in the context of which the artefact was produced. Especially for
  EC-funded projects, the Grant Agreement number.
\item Links to other copies (e.g., copies archived in arxiv or Zenodo)
  of the same artefact.
\end{itemize}
%
The NOS-defined metadata items for scientific articles, technical
reports, and, in general, documents are the following:
%
\begin{itemize}
\item In addition to the simpler metadata (creators, date, title)
  provided for artefacts of any type, full bibiliographic reference
  (venue, journal, collection, etc.) for self-archived copies of
  previously published documents; especially the DOI of the version of
  record.
\item Links to datasets and software used for the analyses that are
  part of the research presented in the document.
\end{itemize}
%
The NOS-defined metadata items for datasets are the following:
%
\begin{itemize}
\item A sample of the data. This can be a few seconds of a video, a
  few lines of tabular data, a few images from a larger collection,
  etc. The essential property of the sample is that it is
  syntactically identical to the full dataset, so that compatibility
  with software can be checked without downloading the full dataset.
  The sample is referencable dataset in its own right, and does not
  need to literally be part of the full dataset, so that multiple
  datasets can point to the same sample if they are syntactically
  identical.
\item Technical information about the type or types of the data
  (images, video, numerical, etc.) and the file format.
\item Links to documents that explain how the consistency and quality
  of data collection was controlled and documented. This may include
  processes such as calibration, repeated samples or measurements,
  standardized data capture, data entry validation, peer review of
  data, or representation with controlled vocabularies.
\item Links to documents presenting research that included analysing
  this dataset.
\item Links to the software used to generate the data.
\item Links to software that is able to parse and analyse the data.
\end{itemize}
%
The NOS-defined metadata items for artefacts that are representations
or measurements of physical objects kept in laboratories (e.g., blood
samples) are the following:
%
\begin{itemize}
\item Links to the relevant laboratory catalogue.
\end{itemize}
%
The NOS-defined metadata items for artefacts that fall within the
scope of the GDPR are the following:
%
\begin{itemize}
\item Permitted purposes of any subsequent processing.
\item Expiration date for any subsequent processing.
\end{itemize}

\comment{Stasinos}{There is no consensus about specific vocabularies,
  but an initial decision needs to be made and recorded here.}

\comment{Stasinos}{The FAIRsFAIR project, started in 2019, is a
  European effort aiming to provide practical solutions for the use of
  the FAIR data principles throughout the research data lifecycle. See
  D2.5, FAIR Semantics Recommendations Second Iteration, DOI:10.5281/zenodo.5362010}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Sharing and Access Controls}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

As the main purpose of the NOS infrastructure is to facilitate open
science, technically it is also geared towards the open publishing of
the artefacts it holds. It does, however, also support publishing
metadata about artefacts that are only avaible under license or
NCSR-internal. In this latter case the published metadata aim to
disseminate the existence of the closed data and make known the
license, NDA, or other terms under which they can be shared.

The unique identifier assigned to each artefact resolves to an HTTPS
server that provides the human-readable Web page or machine-readable
metadata record of the artefact.\footnote{Both have identical
  information. The client resolving the identifier requests the
  human-readable or machine-readable version through the normal
  content negotiation mechanism of the HTTPS protocol.}
This page or record includes a resolvable URL through which the
artefact can be downloaded without requiring any human interaction.
This URL is a systematic derivation (such as a constant suffix or
argument) of the identifier and can be used to directly download the
artefact bypassing the metadata page.

\begin{callout}
Identifiers are sufficient to download the data without any further
interaction, which is one of the accessibility requirements.
\end{callout}

The NOS infrastructure authenticates users via GRNET's Delos identity
service.\footnote{See also \url{https://grnet.gr/services/delos}}
Delos uses the Shibboleth server which complies with the OASIS
SAML~2.0 protocol.\footnote{See also
  \url{https://wiki.oasis-open.org/security}}
Only authenticated users who are known to Delos as members of \ncsr
can upload content.

\begin{callout}
Latest standards are applied in user authentication. In order to
safeguard the repositories' institutional character, content providers
must be authenticated NCSR researchers. Naturally this is not a
sufficient condition: Users must also adhere to the policy of
publishing the outputs of research carried out at \ncsr projects.
\end{callout}

\comment{Stasinos}{Interoperability with the Gitlab identity service,
  Zenodo, CKAN, DSpace, and query processors to be checked.}

Users are not able to edit previously uploaded content except through
updates that are recorded as versions with their own unique
identifier. If it becomes necessary for technical (e.g., an error has
been fixed that makes the previous version unuseable) or other reasons
(e.g., consent or license has been revoked) to remove a dataset or
some of its versions, the identifier remains resolvable and the
metadata record persists, notifying the user that this artefact is not
longer available.

\begin{callout}
One of the requirements for trusted repositories is that identifiers
are persistent, with updates marked as distinct versions. Also, that
identifiers and metadata are accessible, even when the data is not.
\end{callout}
