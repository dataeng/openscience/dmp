\chapterimage{head_objectives.pdf}
\chapter{Objectives}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Information Sources and Core Principles}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This document proposes policies regarding the management of digital
artefacts (datasets, software, articles and reports) that are either
used in or produced by research conducted at \ncsr. The proposed
policies aim to ensure the alignment of the practices applied at \ncsr
with current international best practices, requirements of funding
bodies, and obligations stemming from the GDPR
and the overal EU digital strategy suite.

\textit{FAIR Guiding Principles for scientific data management
  and stewardship}\footnote{M. Wilkinson, M. Dumontier, I.,
  Aalbersberg et al., The FAIR Guiding Principles for
  scientific data management and stewardship. Sci Data 3, 160018 (2016).
  \doi{10.1038/sdata.2016.18}}
is probably the single most influential document on best practices for
science data management. In short, the FAIR principles stipulate that
data should be:
%
\begin{itemize}
\item Findable: (meta)data are assigned a globally unique and
  persistent identifier; data are described with rich metadata
  (defined in \emph{Reusable} below); metadata clearly and explicitly
  include the identifier of the data it describes; (meta)data are
  registered or indexed in a searchable resource.
\item Accessible: (meta)data are retrievable by their identifier using
  a standardized communications protocol; the protocol is open, free,
  and universally implementable; the protocol allows for an
  authentication and authorization procedure, where necessary;
  metadata are accessible, even when the data are no longer available.
\item Interoperable: (meta)data use a formal, accessible, shared, and
  broadly applicable language for knowledge representation; (meta)data
  use vocabularies that follow FAIR principles; (meta)data include
  qualified references to other (meta)data.
\item Reusable: meta(data) are richly described with a plurality of
  accurate and relevant attributes; (meta)data are released with a
  clear and accessible data usage license; (meta)data are associated
  with detailed provenance; (meta)data meet domain-relevant community
  standands.
\end{itemize}
%
The FAIR principles have been widely endorsed by the scientific
community, the Research Data Alliance Foundation (RDA), and the
European Commission and have become the basis for various concrete
recommendations and obligations.

The RDA established in 2019 the
\textit{FAIR Data Maturity Model Working Group} with the aim to
publish an RDA Recommendation for a common set of core assessment
criteria for FAIRness.\footnote{See also
  \url{https://www.rd-alliance.org/groups/fair-data-maturity-model-wg}}
This recommendation will provide concrete guidance on how to publish
research data in accordance with the (more abstract) FAIR principles
and how to evaluate the FAIR-wise maturity of research data. To this
date the working group has produced a first set of guidelines and a
checklist related to the implementation of the
indicators.\footnote{FAIR Data Maturity Model Working Group, FAIR Data
  Maturity Model: Specification and Guidelines. June 2020.
  \doi{10.15497/rda00050}}
In the Model Grant Agreement of the Horizon~Europe
programme,\footnote{See also the Annotated Grant Agreement,
  Draft v0.2, November 2021, and specifically Annex 5,
  \quotes{H-E Communication, Dissemination, Open Science and Visibility},
  Section~2 \quotes{Open Science}.
  Available at \url{https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/common/guidance/aga\_en.pdf}}
the European Commission makes it explicit that the beneficiaries must
manage the digital research data generated in the action
responsibly, in line with the FAIR principles. The Model Grant
Agreement also describes a series of actions that must be
implemented to this effect, including preparing a initial
\term{data management plan (DMP)} and updating it throughout the
project, depositing data and publications in
\term{trusted repositories}, and annotating data and publications with
specific metadata. In order to support with the development, update,
and execution of the DMP the Commission provides a DMP
template\footnote{Available at
  \url{https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/horizon/temp-form/report/data-management-plan-template_he_en.docx}}
as well as recommendations on the implementation of FAIR Practices.\footnote{
  Prepared by the \textit{FAIR in Practice} Task Force
  of the \textit{FAIR Working Group} of the European Open Science Cloud.
  Available at 
  \url{https://ec.europa.eu/info/sites/default/files/research_and_innovation/ki0120580enn.pdf}}

In Greece, a working group representing 11 universities and research
centres and 26 research infrastructures and initiatives drafted a
proposal for a \term{National Open Science Policy}.\footnote{Available
  at \url{https://zenodo.org/record/3908953}}
The policy draft identifies open publications, open data, and open
software as the components of open science and also adopts the FAIR
principles for their management and publication. Although not a
formal policy document at this stage, it is expected that national
research funding will align its formal policies with those of the EC,
to also require having a DMP and assuming open and FAIR science as the
default position.

In addition to the FAIR principles discussed above, ethical and legal
obligations are also in place when collecting, storing, and processing
personal data. Specifically, use of personal data is regulated by the
\term{General Data Protection Regulation (GDPR)}.\footnote{Regulation
  (EU) 2016/679, available at
  \url{https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX\%3A02016R0679-20160504}}
For NCSR projects, compliance to the GDPR is ensured through the
Ethics Committee and through NCSR's internal
personal data protection policy.\footnote{Only interally
  accessible at \url{https://admin.demokritos.gr/kanones-leitourgias}}
Projects that make use of personal data need to provide to the
Ethics Committee a description of the processes applied in order to
ensure GDPR compliance.

\comment{Stasinos}{Contact NCSR Ethics Committee: Confirm that
  complying with the GDPR is the only requirement. See if there
  is a template for how the project reports GDPR compliance.}

\comment{Alexandros}{In theory the ethics committee should provide
  guidance or simply approve AI methodologies, dataset used, proof of
  concept in regard to transparency, risk assessments under the AI
  Regulation Proposal}

\comment{Stasinos}{Contact DPO about specifics on GDPR compliance and
  the internal NCSR policy.}

\comment{Alexandros}{It would be nice to obtain a practical guidance
  for the following:
  (1) choosing legal bases for the various tasks within the research
  project (legal basis here is not limited to scientific research;
  (2) pre-DPIA and DPIA templates focused for research projects that
  include personal data.
  (3) Consent forms templates for given tasks following the institutes
  needs and researches (eg for participation in clinical trials,
  surveys, general in vivo experiments)}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{NCSR Open Science Infrastructure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The policies in this document are complemented by the
\term{NCSR Open Science Infrastructure (NOS)},
an IT system centrally developed and maintained by \ncsr.
NOS is envisaged to be organized as four interoperable systems
covering the following use cases:
%
\begin{itemize}
\item The core systems are \term{NOS-Data} and \term{NOS-Pub},
  institutional repositories for datasets and publications,
  respectively. They can be used to publish original research datasets
  and technical reports or other previously unpublished technical
  documents, with links between the two so that the reports document
  the reserach that produced of used these datasets.
\item NOS-Pub can also serve as an institutional document repository
  for sharing pre-prints and author-prepared accepted manuscripts of
  published articles, depending on author rights for the different
  journals and publication models.
\item In order to fully document scientific experiments, it is often
  necessary to also link data and reports and articles with the
  software used to process the data. The \term{NOS-Code} institutional
  code repository can be used for publishing specific versions of
  software used in experiments. NOS-Code accommodates both source code
  and binary executables; and, especially for the latter, their
  packaging in the appropriate execution environment (e.g., specific
  versions of libraries). This allows scientistists to archive the
  complete setup of a computational experiment to ensure
  reproducibility in the face of software updates or complete
  unavailability in the future.
\item Finally, \term{NOS-Services} is an infrastructure for
  provisioning the execution of software from NOS-Code over data from
  NOS-Data to provide data access services that are more sophisticated
  than finding and downloading datasets. Such services can be deep
  querying for specific slices of the overall dataset or more complex
  aggregation or other processing of the data. Especially for
  voluminous datasets, providing such services can prove to be the
  only sustainable way for large datasets to remain useful.
\item Although the primary function of all of the above is expected to
  be the public sharing of research items, they can also be used for
  the private archiving and restricted, authenticated access of
  datasets, documents, and software for cases where unrestricted,
  public access is not permitted.
\end{itemize}
%
This infrastructure aims to make it as effortless as possible for NCSR
researchers to implement principles and policies that are
internationally considered best practice, required by funding bodies,
or legal and ethical obligations. At the same time, it is the
researchers themselves that must implement the policies using the
infrastructure.

\begin{callout}
This document presents an interpretation of the FAIR principles (one
of many possible interpretations) and an IT system that aims to
support in implementing these principles under this interpretation.
Both this document and the IT system are provided in the hope that
they will be useful.
\end{callout}

Of these systems, NOS-Pub and NOS-Data will be deployed first, as
these cover the most critical aspects of FAIRness and GDPR compliance.
NOS-Code and NOS-Services are planned for a later phase to greatly
enhance the re-usability and interoperability of the data, as well as
the sustainability of the data repository at larger data scales.

\comment{Alexandros}{In regard to personal data DPO could
  formulate/provide a relevant decision tree here. Decision trees
  could stand to reason for the rest as well. Mention that
  it will be make easy to apply a licensing suite like CC.}

\comment{Alexandros}{I would not limit the scope to the GDPR as non
  personal data apply as well. As such I would also include the Data
  Governance Act and the Data Act.}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Relation to DMP and data maturity indicators}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The Data Protection Officer (DPO) of \ncsr is responsible for drafting
each project's DMP and for supervising its implementation. The
principle investigator (PI) of the project is responsible for
providing the DPO with all project-specific parameters and details and
for provisioning in the project budget the resources needed to
implement the DMP.

\comment{Alexandros}{Double-check the above, not accurate.}

This document cannot substitute the DMP; similarly, NOS cannot
substitute researchers' effort for implementing the DMP and, in
general, for adhering to best practices. This document aims to guide
researchers to best practices regarding data management and to clarify
what is provided by NOS and what remains the responsibility of the PI
and the project team in order to implement these best practices.

\begin{callout}
Covering some of the sections foreseen in the Commission's DMP
template is one of the objectives of the policies described here and
of the NOS infrastructure that facilitates these policies's
implementation. A partially-filled EC DMP template is provided as an
appendix to this document. The text in this document and in the
appendix can be used by NCSR researchers to prepare their projects'
DMP, naturally assuming NOS is used and these policies are adhered to.
\end{callout}

RDA data maturity indicators offer guidelines and checklists that,
similarly to the Commission's guidelines and DMP template, specify
actions at a more concrete level than the original FAIR principles. By
contrast to the Commission's resources, the RDA indicators aim to
evaluate the FAIR-wise maturity of research data and to offer
guidelines about how to increase these indicators. It is expected that
data maturity indicators (by the RDA working work or similar future
initiatives) will become an important metric in assessing a project's
dissemination and impact-creation activities.

\begin{callout}
Covering some of the RDA data maturity indicators is one of the
objectives of the policies described here and of the NOS
infrastructure that facilitates these policies's implementation. Text
provided in this document regarding maturity indicators can be freely
used by researchers implementing these policies and using this digital
infrastructure.
\end{callout}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Relation to Ethical and Legal Requirements}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This policy document will be shared with the Ethics Committee
and with the DPO for their comments and consideration.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Relation to Other Initiatives}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

HELIX is the Greek national eInfrastructure for data-intensive
research, handling the data management, analysis, sharing, and reuse
needs of Greek scientists, researchers and innovators in a
cross-disciplinary, scalable, and low-cost manner.\footnote{See
 also \url{https://hellenicdataservice.gr}}
HELIX is both a repositository, deployed at GRNET's Okeanos-Knossos
computing infrastructure, and an aggregator and catalogue of data
published at other repositories. Publications metadata is harvested
from OpenAIRE as well as national and institutional Open Access
repositories. Data is either reposited at HELIX by users or harvested
from institutional data catalogues and repositories, as well as public
data catalogues and crowdsourcing initiatives.

At the current implementation phase (Phase 2) HELIX has not scaled out
its internal repository and cannot be used as a data and publications
repository. It is, however, the aim that the contents of the NCSR
institutional repository will be aggregated in HELIX catalogues.

\comment{Stasinos}{OpenAIRE. EOSC.}

